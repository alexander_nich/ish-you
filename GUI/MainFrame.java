package gui;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame{
	
	public MainFrame(){
		super("ISH-YOU");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container container = new Container();
		getContentPane().add(container);
		pack();
		setVisible(true);
	}
	
}

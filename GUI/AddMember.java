package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import exceptions.CannotConnectException;
import utils.DatabaseManager;
import utils.SQLiteDatabaseManager;

@SuppressWarnings("serial")
public class AddMember extends JPanel{
	private JPanel emailPanel, usernamePanel, passwordPanel, submitPanel;
	private JLabel addMemberLabel, emailLabel, usernameLabel, passwordLabel;
	private JTextField emailTextField, usernameTextField, passwordTextField;
	private JButton submitNewUserButton;
	
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	
	public AddMember(){
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		addMemberLabel = new JLabel("Create a new Member to access Issues");
		addMemberLabel.setFont(addMemberLabel.getFont().deriveFont(24f));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		emailPanel = new JPanel();
		emailPanel.setPreferredSize(new Dimension(500,50));
		emailPanel.setBorder(BorderFactory.createTitledBorder("Send User's Log In info"));
		emailLabel = new JLabel("User's email address");
		emailTextField = new JTextField(30);
		emailPanel.add(emailLabel);
		emailPanel.add(emailTextField);
		
		usernamePanel = new JPanel();
		usernamePanel.setPreferredSize(new Dimension(500,50));
		usernamePanel.setBorder(BorderFactory.createTitledBorder("Set User's Username"));
		usernameLabel = new JLabel("User Name to be created");
		usernameTextField = new JTextField(20);
		usernamePanel.add(usernameLabel);
		usernamePanel.add(usernameTextField);
		
		passwordPanel = new JPanel();
		passwordPanel.setPreferredSize(new Dimension(500,50));
		passwordPanel.setBorder(BorderFactory.createTitledBorder("Set User's Password"));
		passwordLabel = new JLabel("User's password to be used");
		passwordTextField = new JTextField(20);
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordTextField);
		
		submitPanel = new JPanel();
		submitPanel.setPreferredSize(new Dimension(100,50));
		ButtonListener buttonListener = new ButtonListener();
		submitNewUserButton = new JButton("Create User");
		submitNewUserButton.addActionListener(buttonListener);
		submitPanel.add(submitNewUserButton);
		
		add(addMemberLabel);
		add(Box.createRigidArea(new Dimension(0,10)));
		add(emailPanel);
		add(Box.createVerticalGlue());
		add(usernamePanel);
		add(Box.createVerticalGlue());
		add(passwordPanel);
		add(Box.createRigidArea(new Dimension(10,25)));
		add(submitPanel);
	}
	
	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == submitNewUserButton){
//				String userEmail = emailTextField.getText();
				String userName = usernameTextField.getText();
				String userPass = passwordTextField.getText();
				
				try {
					DatabaseManager database = new SQLiteDatabaseManager();
					database.createUser(userName, userPass, 0);
				} catch (CannotConnectException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Call createUser() with userEmail, userName, userPass as parameters
			}
			
		}
		
	}
}
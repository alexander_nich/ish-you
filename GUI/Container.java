package gui;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Container extends JPanel{

	private static BaseScreen baseScreen;
	private static AsideScreen asideScreen;
	private HomeScreen homeScreen;
	private CreateIssue createIssue;
	private IssueLog issueLog;
	private AddMember addMember;
	private final int WIDTH = 1000;
	private final int HEIGHT = 600;
	private final static String LOGIN_SCREEN = "Login panel";
	private final static String MENU_SCREEN = "Menu panel";
	private final static String HOME_SCREEN = "Home panel";
	private final static String CREATE_SCREEN = "Create Issue panel";
	private final static String ISSUE_LOG = "Issue Log panel";
	private final static String ADD_MEMBER = "Add Member panel";
	private final static String VIEW_MESSAGES = "Messages panel";
	
public Container(){
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setLayout(new BorderLayout());
		
		baseScreen = new BaseScreen(new CardLayout());
		baseScreen.add(new LoginScreen(), LOGIN_SCREEN);
		baseScreen.setBorder(BorderFactory.createEtchedBorder());
		
		asideScreen = new AsideScreen(new CardLayout());
		asideScreen.add(new MenuScreen(), MENU_SCREEN);
		asideScreen.setBorder(BorderFactory.createEtchedBorder());

		add(baseScreen, BorderLayout.CENTER);
		add(asideScreen, BorderLayout.WEST);

		/*aside.setVisible(false);*/ //Leave commented for testing purposes
	}
	
	public static void login(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new LoginScreen(), LOGIN_SCREEN);
		asideScreen = new AsideScreen(new CardLayout());
		asideScreen.add(new MenuScreen(), MENU_SCREEN);
		asideScreen.setBorder(BorderFactory.createEtchedBorder());
//		add(asideScreen, BorderLayout.WEST);
		cl.show(baseScreen, LOGIN_SCREEN);		
	}
	
	public static void homePage(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new HomeScreen(), HOME_SCREEN);
		cl.show(baseScreen, HOME_SCREEN);
	}
	
	public static void addMember(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new AddMember(), ADD_MEMBER);
		cl.show(baseScreen, ADD_MEMBER);
	}
	
	public static void issueLog(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new IssueLog(), ISSUE_LOG);
		cl.show(baseScreen, ISSUE_LOG);
	}
	
	public static void createIssue(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new CreateIssue(), CREATE_SCREEN);
		cl.show(baseScreen, CREATE_SCREEN);
	}
	
	public static void viewMessages(){
		CardLayout cl = (CardLayout) baseScreen.getLayout();
		baseScreen.add(new ViewMessages(), VIEW_MESSAGES);
		cl.show(baseScreen, VIEW_MESSAGES);
	}
	
}

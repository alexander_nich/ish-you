package gui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuScreen extends JPanel{

	private JLabel menuLabel;
	private JPanel homePanel,createIssuePanel, addMemberPanel, viewMessagesPanel, issueLogPanel;
	
	public MenuScreen(){
		
		setPreferredSize(new Dimension(300, 600));
		setBackground(Color.white);
		menuLabel = new JLabel("Side-Menu");
		menuLabel.setFont(menuLabel.getFont().deriveFont(16f));
		add(menuLabel);
		
		createIssuePanel = new JPanel();
		createIssuePanel.setPreferredSize(new Dimension(300,100));
		createIssuePanel.setBorder(BorderFactory.createRaisedBevelBorder());
		createIssuePanel.add(new JLabel("Create Issue"));
		createIssuePanel.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				createIssuePanel.setBorder(BorderFactory.createEtchedBorder());
			}
			public void mouseClicked(MouseEvent e){
				createIssuePanel.setBorder(BorderFactory.createLoweredBevelBorder());
				Container.createIssue();
			}
			public void mouseExited(MouseEvent e){
				createIssuePanel.setBorder(BorderFactory.createRaisedBevelBorder());
			}
		});
		
		addMemberPanel = new JPanel();
		addMemberPanel.setPreferredSize(new Dimension(300,100));
		addMemberPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		addMemberPanel.add(new JLabel("Add Member"));
		addMemberPanel.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				addMemberPanel.setBorder(BorderFactory.createEtchedBorder());
			}
			public void mouseClicked(MouseEvent e){
				addMemberPanel.setBorder(BorderFactory.createLoweredBevelBorder());
//				JOptionPane.showMessageDialog(null, "Mouse click test");
				Container.addMember();
				//Open Member adding screen
			}
			public void mouseExited(MouseEvent e){
				addMemberPanel.setBorder(BorderFactory.createRaisedBevelBorder());
			}
		});
		
		viewMessagesPanel = new JPanel();
		viewMessagesPanel.setPreferredSize(new Dimension(300,100));
		viewMessagesPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		viewMessagesPanel.add(new JLabel("View Messages"));
		viewMessagesPanel.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				viewMessagesPanel.setBorder(BorderFactory.createEtchedBorder());
			}
			public void mouseClicked(MouseEvent e){
				viewMessagesPanel.setBorder(BorderFactory.createLoweredBevelBorder());
				Container.viewMessages();
				//Open message screen
			}
			public void mouseExited(MouseEvent e){
				viewMessagesPanel.setBorder(BorderFactory.createRaisedBevelBorder());
			}
		});
		
		issueLogPanel = new JPanel();
		issueLogPanel.setPreferredSize(new Dimension(300,100));
		issueLogPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		issueLogPanel.add(new JLabel("Issue Log"));
		issueLogPanel.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				issueLogPanel.setBorder(BorderFactory.createEtchedBorder());
			}
			public void mouseClicked(MouseEvent e){
				issueLogPanel.setBorder(BorderFactory.createLoweredBevelBorder());
//				JOptionPane.showMessageDialog(null, "Mouse click test");
				Container.issueLog();
				//Open Issue log screen
			}
			public void mouseExited(MouseEvent e){
				issueLogPanel.setBorder(BorderFactory.createRaisedBevelBorder());
			}
		});
		
		homePanel = new JPanel();
		homePanel.setPreferredSize(new Dimension(300, 40));
		homePanel.setBorder(BorderFactory.createRaisedBevelBorder());
		homePanel.setBackground(Color.white);
		homePanel.add(new JLabel("Home"));
		homePanel.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				homePanel.setBorder(BorderFactory.createEtchedBorder());
			}
			public void mouseClicked(MouseEvent e){
				homePanel.setBorder(BorderFactory.createLoweredBevelBorder());
//				JOptionPane.showMessageDialog(null, "Mouse click test");
				Container.homePage();
				//Open Issue log screen
			}
			public void mouseExited(MouseEvent e){
				homePanel.setBorder(BorderFactory.createRaisedBevelBorder());
			}
		});
		
		add(homePanel);
		add(createIssuePanel);
		add(addMemberPanel);
		add(viewMessagesPanel);
		add(issueLogPanel);
		
		
	}
	
}

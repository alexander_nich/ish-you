package gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import utils.DatabaseManager;
import utils.SQLiteDatabaseManager;

@SuppressWarnings("serial")
public class LoginScreen extends JPanel{

	private JLabel usernameLabel, passwordLabel;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	
	public LoginScreen(){
		
		usernameLabel = new JLabel("Username");
		add(usernameLabel);
		usernameField = new JTextField(20);
		add(usernameField);
		
		passwordLabel = new JLabel("Password");
		add(passwordLabel);
		passwordField = new JPasswordField(20);
		add(passwordField);
		
		ButtonListener buttonListener = new ButtonListener();
		
		loginButton = new JButton("Login");
		loginButton.addActionListener(buttonListener);
		add(loginButton);
	}
	
	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource() == loginButton){
				String adminN = new String("ishyou"); //Admin log in info
				String adminP = new String("lol"); //Admin log in info
				
				String userN = usernameField.getText();
				char[] passW = passwordField.getPassword();
				String passString = new String(passW);
				if(userN.isEmpty() || passW.length == 0){
					JOptionPane.showMessageDialog(null, "The Username and/or Password field(s) is/are not filled in !", "Warning",
							JOptionPane.WARNING_MESSAGE);
				}
				else if(userN.equals(adminN) && passString.equals(adminP)){
						JOptionPane.showMessageDialog(null, "Logging in");
						Container.homePage();
				}	
			}
		}
	}
}
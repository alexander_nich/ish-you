package gui;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import utils.DatabaseManager;
import utils.SQLiteDatabaseManager;
import domain.Issue;
import exceptions.CannotConnectException;

@SuppressWarnings("serial")
public class HomeScreen extends JPanel{
	
	private JLabel homeLabel, recentIssuesLabel;
	private JPanel titlePanel, recentIssuesPanel, rIssue1Panel, rIssue2Panel, rIssue3Panel, rIssue4Panel;
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	
	public HomeScreen(){
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setBackground(Color.white);
		homeLabel = new JLabel("Home Page");
		homeLabel.setFont(homeLabel.getFont().deriveFont(24f));
		add(homeLabel);
		
		titlePanel = new JPanel();
		titlePanel.setPreferredSize(new Dimension(WIDTH, 50));
		titlePanel.setBorder(BorderFactory.createEtchedBorder());
		recentIssuesLabel = new JLabel("Recently Added Issues");
		recentIssuesLabel.setFont(recentIssuesLabel.getFont().deriveFont(18));
		titlePanel.add(recentIssuesLabel);
		
		recentIssuesPanel = new JPanel();
		recentIssuesPanel.setPreferredSize(new Dimension(WIDTH,600));
		recentIssuesPanel.setBorder(BorderFactory.createEtchedBorder());
		
		// Some method to determine recent additions
		// Most likely do by Issue ID num
		// Get Issue IDs in database with for loop
		// If IssueID > Issue ID before, determine as most recent
		
		try {
			DatabaseManager database = new SQLiteDatabaseManager();
			ArrayList<Issue> issues = database.getAllIssues();
			for (Issue issue : issues){
				rIssue1Panel = new JPanel();
				rIssue1Panel.setBorder(BorderFactory.createEtchedBorder());
				rIssue1Panel.setLayout(new BorderLayout());
				rIssue1Panel.setPreferredSize(new Dimension(WIDTH,100));
				rIssue1Panel.add(new JLabel(issue.getTitle()), BorderLayout.NORTH);
				rIssue1Panel.add(new JLabel(issue.getContent()), BorderLayout.SOUTH);
				rIssue1Panel.addMouseListener(new MouseAdapter(){
					public void mouseClicked(MouseEvent e){
						rIssue1Panel.setBorder(BorderFactory.createLoweredBevelBorder());
						Object[] options = {"Edit Issue","Comment On Issue","Delete Issue"};
						int i = JOptionPane.showOptionDialog(null, "What would you like to do with this Issue?",
								"getIssueID()", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
								options, options[2]);
						if(i == 0){
							JOptionPane.showMessageDialog(null, "Open Issue Editing Mini Window");
						}
						if(i == 1){
							JOptionPane.showMessageDialog(null, "Open Issue Commenting Mini Window");
						}
						if(i ==2){
							JOptionPane.showMessageDialog(null, "deleteIssue() from database");
						}
						//Open Issue 1 Info screen
					}
					public void mouseExited(MouseEvent e){
						rIssue1Panel.setBorder(BorderFactory.createEtchedBorder());
					}
				});
				
//				rIssue2Panel = new JPanel();
//				rIssue2Panel.setBorder(BorderFactory.createEtchedBorder());
//				rIssue2Panel.setLayout(new BorderLayout());
//				rIssue2Panel.setPreferredSize(new Dimension(WIDTH,100));
//				rIssue2Panel.add(new JLabel(issue.getTitle()), BorderLayout.NORTH);
//				rIssue2Panel.add(new JLabel(issue.getContent()), BorderLayout.SOUTH);
//				rIssue2Panel.addMouseListener(new MouseAdapter(){
//					public void mouseClicked(MouseEvent e){
//						rIssue2Panel.setBorder(BorderFactory.createLoweredBevelBorder());
//						Object[] options = {"Edit Issue","Comment On Issue","Delete Issue"};
//						int i = JOptionPane.showOptionDialog(null, "What would you like to do with this Issue?",
//								"getIssueID()", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
//								options, options[2]);
//						if(i == 0){
//							JOptionPane.showMessageDialog(null, "Open Issue Editing Mini Window");
//						}
//						if(i == 1){
//							JOptionPane.showMessageDialog(null, "Open Issue Commenting Mini Window");
//						}
//						if(i ==2){
//							JOptionPane.showMessageDialog(null, "deleteIssue() from database");
//						}
//						//Open Issue 2 Info screen
//					}
//					public void mouseExited(MouseEvent e){
//						rIssue2Panel.setBorder(BorderFactory.createEtchedBorder());
//					}
//				});
//				
//				rIssue3Panel = new JPanel();
//				rIssue3Panel.setBorder(BorderFactory.createEtchedBorder());
//				rIssue3Panel.setLayout(new BorderLayout());
//				rIssue3Panel.setPreferredSize(new Dimension(WIDTH,100));
//				rIssue3Panel.add(new JLabel(issue.getTitle()), BorderLayout.NORTH);
//				rIssue3Panel.add(new JLabel(issue.getContent()), BorderLayout.SOUTH);
//				rIssue3Panel.addMouseListener(new MouseAdapter(){
//					public void mouseClicked(MouseEvent e){
//						rIssue3Panel.setBorder(BorderFactory.createLoweredBevelBorder());
//						Object[] options = {"Edit Issue","Comment On Issue","Delete Issue"};
//						int i = JOptionPane.showOptionDialog(null, "What would you like to do with this Issue?",
//								"getIssueID()", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
//								options, options[2]);
//						if(i == 0){
//							JOptionPane.showMessageDialog(null, "Open Issue Editing Mini Window");
//						}
//						if(i == 1){
//							JOptionPane.showMessageDialog(null, "Open Issue Commenting Mini Window");
//						}
//						if(i ==2){
//							JOptionPane.showMessageDialog(null, "deleteIssue() from database");
//						}
//						//Open Issue 3 Info screen
//					}
//					public void mouseExited(MouseEvent e){
//						rIssue3Panel.setBorder(BorderFactory.createEtchedBorder());
//					}
//				});
//				
//				rIssue4Panel = new JPanel();
//				rIssue4Panel.setBorder(BorderFactory.createEtchedBorder());
//				rIssue4Panel.setLayout(new BorderLayout());
//				rIssue4Panel.setPreferredSize(new Dimension(WIDTH,100));
//				rIssue4Panel.add(new JLabel(issue.getTitle()), BorderLayout.NORTH);
//				rIssue4Panel.add(new JLabel(issue.getContent()), BorderLayout.SOUTH);
//				rIssue4Panel.addMouseListener(new MouseAdapter(){
//					public void mouseClicked(MouseEvent e){
//						rIssue4Panel.setBorder(BorderFactory.createLoweredBevelBorder());
//						Object[] options = {"Edit Issue","Comment On Issue","Delete Issue"};
//						int i = JOptionPane.showOptionDialog(null, "What would you like to do with this Issue?",
//								"getIssueID()", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
//								options, options[2]);
//						if(i == 0){
//							JOptionPane.showMessageDialog(null, "Open Issue Editing Mini Window");
//						}
//						if(i == 1){
//							JOptionPane.showMessageDialog(null, "Open Issue Commenting Mini Window");
//						}
//						if(i ==2){
//							JOptionPane.showMessageDialog(null, "deleteIssue() from database");
//						}
//						//Open Issue 4 Info screen
//					}
//					public void mouseExited(MouseEvent e){
//						rIssue4Panel.setBorder(BorderFactory.createEtchedBorder());
//					}
//				});
			}
			
		} catch (CannotConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		add(titlePanel);
		recentIssuesPanel.add(rIssue1Panel);
//		recentIssuesPanel.add(rIssue2Panel);
//		recentIssuesPanel.add(rIssue3Panel);
//		recentIssuesPanel.add(rIssue4Panel);
		add(recentIssuesPanel);
	}
	
}

package gui;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BaseScreen extends JPanel {
	
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	
	public BaseScreen(CardLayout cardLayout){
		super(cardLayout);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
	}
	
	

}

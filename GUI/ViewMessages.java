package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ViewMessages extends JPanel{
	private JLabel viewMessagesLabel, messengersLabel;
	private JPanel sendMessagePanel, messengersPanel, titlePanel, member1Panel, typeAndSendPanel, member2Panel;
	private JTextField messageField;
	private JButton sendMessage;
	private JTextArea messagesArea;
	
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	
	
	
	public ViewMessages(){
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setLayout(new BorderLayout());
		titlePanel = new JPanel();
		titlePanel.setPreferredSize(new Dimension(WIDTH,50));
		titlePanel.setBorder(BorderFactory.createEtchedBorder());
		viewMessagesLabel = new JLabel("View Messages");
		viewMessagesLabel.setFont(viewMessagesLabel.getFont().deriveFont(24f));
		titlePanel.add(viewMessagesLabel);
		
		
		messengersLabel = new JLabel("Members");
		messengersPanel = new JPanel();
		messengersPanel.add(messengersLabel);
		
		member1Panel = new JPanel();
		member1Panel.setBorder(BorderFactory.createEtchedBorder());
		member1Panel.setLayout(new BorderLayout());
		member1Panel.setPreferredSize(new Dimension(250,100));
		member1Panel.add(new JLabel("Edwards, Howard"), BorderLayout.CENTER);
		
//		member2Panel = new JPanel();
//		member2Panel.setBorder(BorderFactory.createEtchedBorder());
//		member2Panel.setLayout(new BorderLayout());
//		member2Panel.setPreferredSize(new Dimension(250,100));
//		member2Panel.add(new JLabel("Nicholson, Alexander"), BorderLayout.CENTER);
		
		sendMessagePanel = new JPanel();
		sendMessagePanel.setBorder(BorderFactory.createEtchedBorder());
		sendMessagePanel.setLayout(new BorderLayout());
		sendMessagePanel.setPreferredSize(new Dimension(440, HEIGHT));
		messagesArea = new JTextArea();
		messagesArea.setLineWrap(true);
		messagesArea.setWrapStyleWord(true);
		
		typeAndSendPanel = new JPanel();
		typeAndSendPanel.setBorder(BorderFactory.createEtchedBorder());
		typeAndSendPanel.setLayout(new BorderLayout());
		typeAndSendPanel.setPreferredSize(new Dimension(440, 100));
		messageField = new JTextField(140);
		messageField.setText("140 Characters maximum in message allowed ...");
		
		ButtonListener buttonListener = new ButtonListener();
		
		sendMessage = new JButton("Send ->");
		sendMessage.addActionListener(buttonListener);
		
		messengersPanel.add(member1Panel);
//		messengersPanel.add(member2Panel);
		
		typeAndSendPanel.add(messageField, BorderLayout.WEST);
//		typeAndSendPanel.add(sendMessage, BorderLayout.EAST);
		
		sendMessagePanel.add(messagesArea, BorderLayout.CENTER);
		sendMessagePanel.add(typeAndSendPanel, BorderLayout.SOUTH);
		
		add(titlePanel, BorderLayout.NORTH);
		add(sendMessagePanel, BorderLayout.EAST);
		add(messengersPanel, BorderLayout.WEST);
		add(sendMessage, BorderLayout.SOUTH);
	}
	
	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == sendMessage){
				
			}
		}
		
	}
}
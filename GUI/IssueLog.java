package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import utils.DatabaseManager;
import utils.SQLiteDatabaseManager;
import domain.Issue;
import exceptions.CannotConnectException;

@SuppressWarnings("serial")
public class IssueLog extends JPanel {
	private JLabel issueLabel;
	
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	
	public IssueLog(){
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		issueLabel = new JLabel("Issue Log");
		add(issueLabel);
		
		JPanel issuesPanel = new JPanel();
		issuesPanel.setPreferredSize(new Dimension(WIDTH,600));
		issuesPanel.setBorder(BorderFactory.createEtchedBorder());
		
		try {
			DatabaseManager database = new SQLiteDatabaseManager();
			ArrayList<Issue> issues = database.getAllIssues();
			for (Issue issue : issues){
				JPanel issuePanel = new JPanel();
				issuePanel.setBorder(BorderFactory.createEtchedBorder());
				issuePanel.setLayout(new BorderLayout());
				issuePanel.setPreferredSize(new Dimension(WIDTH, 100));
				issuePanel.add(new JLabel(issue.getTitle()), BorderLayout.NORTH);
				issuePanel.add(new JLabel(issue.getContent()), BorderLayout.SOUTH);
				issuePanel.addMouseListener(new MouseAdapter(){
					public void mouseClicked(MouseEvent e){
						issuePanel.setBorder(BorderFactory.createLoweredBevelBorder());
						Object[] options = {"Edit Issue","Comment On Issue","Delete Issue"};
						int i = JOptionPane.showOptionDialog(null, "What would you like to do with this Issue?",
								"getIssueID()", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
								options, options[2]);
						if(i == 0){
							String editedContent = (String)JOptionPane.showInputDialog(null, "Edit the issue's description",
									"Edit Issue", JOptionPane.PLAIN_MESSAGE, null, null, null);
							issue.setContent(editedContent);
						}
						if(i == 1){
							JOptionPane.showMessageDialog(null, "Open Issue Commenting Mini Window");
						}
						if(i ==2){
							JOptionPane.showMessageDialog(null, "deleteIssue() from database");
						}
					}
					public void mouseExited(MouseEvent e){
						issuePanel.setBorder(BorderFactory.createEtchedBorder());
					}
				});
				
				add(issuePanel);
			}
		} catch (CannotConnectException e) {
			e.printStackTrace();
		}
		
	}
}
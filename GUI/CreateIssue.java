package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import exceptions.CannotConnectException;
import utils.DatabaseManager;
import utils.SQLiteDatabaseManager;

@SuppressWarnings("serial")
public class CreateIssue extends JPanel{
	private JLabel createIssueLabel, issueTitleLabel;
	private JPanel innerPanel, titlePanel, createIssuePanel;
	private JTextArea issueDescrip;
	private JTextField issueTitle;
	private JButton submitIssue;
	
	private final int WIDTH = 700;
	private final int HEIGHT = 600;
	private final int innerWIDTH = 300;
	private final int innerHEIGHT = 150;
	
	
	public CreateIssue(){
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		createIssueLabel = new JLabel("Creating an Issue. Please provide Issue Description");
		createIssueLabel.setFont(createIssueLabel.getFont().deriveFont(24f));
		add(createIssueLabel);
		
		createIssuePanel = new JPanel();
		createIssuePanel.setBorder(BorderFactory.createEtchedBorder());
		createIssuePanel.setLayout(new BorderLayout());
		createIssuePanel.setPreferredSize(new Dimension(innerWIDTH, 200));
		
		titlePanel = new JPanel();
		titlePanel.setBorder(BorderFactory.createEtchedBorder());
		titlePanel.setLayout(new BorderLayout());
		titlePanel.setPreferredSize(new Dimension(innerWIDTH, 50));
		issueTitleLabel = new JLabel("Title of Issue:");
		issueTitle = new JTextField(20);
		
		innerPanel = new JPanel();
		innerPanel.setBorder(BorderFactory.createEtchedBorder());
		innerPanel.setLayout(new BorderLayout());
		innerPanel.setPreferredSize(new Dimension(innerWIDTH,innerHEIGHT));
		issueDescrip = new JTextArea();
		issueDescrip.setLineWrap(true);
		issueDescrip.setWrapStyleWord(true);
		
		ButtonListener buttonListener = new ButtonListener();
		
		submitIssue = new JButton("Submit Issue");
		submitIssue.addActionListener(buttonListener);
		
		titlePanel.add(issueTitleLabel, BorderLayout.CENTER);
		titlePanel.add(issueTitle, BorderLayout.SOUTH);
		innerPanel.add(issueDescrip, BorderLayout.CENTER);
		innerPanel.add(new JScrollPane(issueDescrip));
		innerPanel.add(submitIssue, BorderLayout.SOUTH);
		createIssuePanel.add(titlePanel, BorderLayout.CENTER);
		createIssuePanel.add(innerPanel, BorderLayout.SOUTH);
		
		add(createIssuePanel);
	}
		
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == submitIssue){
				String description = issueDescrip.getText();
				String title = issueTitle.getText();
				try {
					DatabaseManager database = new SQLiteDatabaseManager();
					database.createIssue(title, description);
					issueDescrip.setText("");
					issueTitle.setText("");
				} catch (CannotConnectException e1) {
					// Show error message					
				}
				//Add IssueDescription string to database
			}
		}
	}

}
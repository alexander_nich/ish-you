package gui;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AsideScreen extends JPanel{
	
	private final int WIDTH = 300;
	private final int HEIGHT = 600;
	
	
	public AsideScreen(CardLayout cardLayout){
		
		super(cardLayout);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		
	}

}

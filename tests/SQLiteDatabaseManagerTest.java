package tests;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import utils.SQLiteDatabaseManager;
import domain.Issue;
import domain.User;
import exceptions.CannotConnectException;
import exceptions.InvalidLoginDetailsException;

public class SQLiteDatabaseManagerTest {

	
	private SQLiteDatabaseManager databaseManager;

	@Before
	public void setUp(){	
		try{
			databaseManager = new SQLiteDatabaseManager();
			databaseManager.connect("test");
			Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");		
			Statement statement = null;
			statement = connection.createStatement();
			String sql = "DROP TABLE IF EXISTS users;" +
						" DROP TABLE IF EXISTS issues;" +
						" DROP TABLE IF EXISTS issues_users;" +
						" CREATE TABLE users " +
	                   " (ID INTEGER PRIMARY KEY     AUTOINCREMENT," +
	                   " USERNAME           TEXT    NOT NULL, " + 
	                   " PASSWORD            TEXT     NOT NULL, " + 
	                   " TEAMID INT NOT NULL);" 
	                   +
	                   " INSERT INTO users (USERNAME, PASSWORD, TEAMID) " +
	                   " VALUES ('Admin', 'Password', 1);" 
	                   +
	                   " CREATE TABLE issues " +
	                   " (ID INTEGER PRIMARY KEY     AUTOINCREMENT," +
	                   " STATUS           TEXT    NOT NULL, " + 
	                   " TITLE            TEXT     NOT NULL, " + 
	                   " CONTENT TEXT NOT NULL);"
	                   + 
	                   " INSERT INTO issues (STATUS, TITLE, CONTENT) " +
	                   " VALUES('INCOMPLETE', '50x errors','Please check the server logs for 50x errors and compile them.');"
	                   +
	                   " INSERT INTO issues (STATUS, TITLE, CONTENT) " +
	                   " VALUES('INCOMPLETE', 'Provision Servers','Please install RHEL on each of the staging machines.');"
	                   +
	                   " CREATE TABLE issues_users " +
	                   " (USER_ID INTEGER NOT NULL," +
	                   " ISSUE_ID INTEGER NOT NULL);" 
	                   +	                   
	                   " INSERT INTO issues_users (USER_ID, ISSUE_ID)" +
	                   " VALUES(1, 1);";
						
			
			statement.executeUpdate(sql);
			statement.close();
		} catch(CannotConnectException  e){
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCanConnectToDataSource() {
		try {
			assertTrue(databaseManager.connect("test"));
		} catch (CannotConnectException e) {
			e.printStackTrace();
		}
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testWontMakeNewDatabaseFile() throws CannotConnectException{
		exception.expect(CannotConnectException.class);
		databaseManager.connect("UNKNOWNDB");
	}
	
	@Test
	public void testAuthenticateReturnsCorrectUser() throws InvalidLoginDetailsException, CannotConnectException{		
			User expected = new User("Admin", "Password", 1, 1);
			User actual = databaseManager.authenticate("Admin", "Password");
			assertEquals(expected, actual);
	}
	
	@Test
	public void testAuthenticateThrowsInvalidLoginExceptionWithIncorrectCredentials() throws InvalidLoginDetailsException, CannotConnectException{
		exception.expect(InvalidLoginDetailsException.class);
		User user = databaseManager.authenticate("nonexistentuser", "nonexistentpassword");
	}
	
	@Test
	public void testCredentialsValidator(){
		assertTrue(databaseManager.isValidCredentials("Admin", "Password"));
	}
	
	@Test
	public void testCredentialsValidatorWontAllowInvalid(){
		assertFalse(databaseManager.isValidCredentials("3admin", "Password") 
				|| databaseManager.isValidCredentials("", "Password")
				|| databaseManager.isValidCredentials("A", "Password")
				|| databaseManager.isValidCredentials("-admins", "password"));
	}
	
	@Test
	public void testCanCreateValidUser() throws CannotConnectException, InvalidLoginDetailsException{
		User expected = new User("validuser","validpassword", 2, 1);
		databaseManager.createUser("validuser", "validpassword", 1);
		User actual = databaseManager.authenticate("validuser", "validpassword");
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testWontCreateInvalidUser() throws CannotConnectException{
		assertFalse(databaseManager.createUser("-invalid-user", "password", 1));
	}
	
	@Test
	public void testCreateUserWontSaveAnInvalidUser() throws CannotConnectException, InvalidLoginDetailsException{
		exception.expect(InvalidLoginDetailsException.class);
		databaseManager.createUser("-invalid-user", "password", 1);
		databaseManager.authenticate("-invalid-user", "password");
	}
	
	@Test
	public void testProtectAgainstSQLInjection() throws CannotConnectException, InvalidLoginDetailsException{
		exception.expect(InvalidLoginDetailsException.class);
		databaseManager.authenticate("1; DELETE * FROM users;", "password");
	}
	
	@Test
	public void testCanGetAllIssues() throws CannotConnectException{
		ArrayList<Issue> expected = new ArrayList<Issue>();
		expected.add(new Issue(1, false, false, "50x errors", "Please check the server logs for 50x errors and compile them."));
		expected.add(new Issue(2, false, false, "Provision Servers", "Please install RHEL on each of the staging machines."));
		ArrayList<Issue> actual = databaseManager.getAllIssues();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAllIssuesGivesEmptyArrayListWithNoIssues() throws SQLException, CannotConnectException{
		Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");		
		Statement statement = null;
		statement = connection.createStatement();
		statement.executeUpdate("DELETE FROM issues;");
		ArrayList<Issue> expected = new ArrayList<Issue>();
		ArrayList<Issue> actual = databaseManager.getAllIssues();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCanCreateAndSaveIssue() throws CannotConnectException{
		databaseManager.createIssue("title", "content");
		ArrayList<Issue> expected = new ArrayList<Issue>();
		expected.add(new Issue(1, false, false, "50x errors", "Please check the server logs for 50x errors and compile them."));
		expected.add(new Issue(2, false, false, "Provision Servers", "Please install RHEL on each of the staging machines."));
		expected.add(new Issue(3, false, false, "title", "content"));
		ArrayList<Issue> actual = databaseManager.getAllIssues();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetIssuesByUser() throws InvalidLoginDetailsException, CannotConnectException {
		ArrayList<Issue> expected = new ArrayList<Issue>();
		expected.add(new Issue(1, false, false, "50x errors", "Please check the server logs for 50x errors and compile them."));
		User admin = databaseManager.authenticate("Admin", "Password");
		ArrayList<Issue> actual = databaseManager.getIssuesByUser(admin);
		assertEquals(expected, actual);
	}	
	
	@Test
	public void testAssociateIssuesWithUsers() throws Exception {
		ArrayList<Issue> expected = new ArrayList<Issue>();
		expected.add(new Issue(1, false, false, "50x errors", "Please check the server logs for 50x errors and compile them."));
		Issue issue = new Issue(2, false, false, "Provision Servers", "Please install RHEL on each of the staging machines.");
		expected.add(issue);
		User admin = databaseManager.authenticate("Admin", "Password");
		databaseManager.associateIssueWithUser(issue, admin);
		ArrayList<Issue> actual = databaseManager.getIssuesByUser(admin);
		assertEquals(expected, actual);
	}
	
}

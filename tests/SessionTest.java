package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import utils.Session;
import domain.User;

public class SessionTest {
	private User admin;
	private Session session;
	
	@Before
	public void setUp(){
		admin = new User("Admin", "Password", 1, 1);
		session = new Session(admin);
	}

	@Test
	public void testValidSessionCanBeCreated(){
		assertTrue(session.isValidSession());
	}
	
	@Test
	public void testSessionContainsUserObject() {
		User expected = admin;
		User actual = session.getUser();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSessionCanBeDeleted(){
		session.delete();
		assertFalse(session.isValidSession());
	}
	
}

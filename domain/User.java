package domain;

public class User {
	
	protected int userID, teamID;
	protected String userName, password;
	
	public User(String userName, String password, int userID, int teamID){
		
		this.userName = userName;
		this.password = password;
		this.userID = userID;
		this.teamID = teamID;
		
	}
	
	
	public int getUserID() {
		return userID;
	}

	public int getTeamID() {
		return teamID;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}


	public boolean equals(Object o){
		if (((User) o).getUserID() == userID && ((User) o).getUserName().equals(userName) && ((User) o).getPassword().equals(password) && ((User) o).getTeamID() == teamID) { 
			return true;
		} else return false;
	}


	@Override
	public String toString() {
		return "User [userID=" + userID + ", teamID=" + teamID + ", userName="
				+ userName + ", password=" + password + "]";
	}

}

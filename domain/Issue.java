package domain;
public class Issue {
	
	private int id;
	private boolean complete, pending;
	private String title, content;
	
	public Issue(int id, boolean complete, boolean pending, String title, String content) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.complete = complete;
		this.pending = pending;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Issue other = (Issue) obj;
		if (complete != other.complete)
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (id != other.id)
			return false;
		if (pending != other.pending)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public int getID() {
		return this.id;
	}

}

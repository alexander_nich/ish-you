package exceptions;

public class CannotConnectException extends Exception {

	public CannotConnectException(){
		super("Cannot connect to data source, please check files.");
	}
	
}

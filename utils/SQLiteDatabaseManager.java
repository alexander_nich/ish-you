package utils;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import domain.Issue;
import domain.User;
import exceptions.CannotConnectException;
import exceptions.InvalidLoginDetailsException;

public class SQLiteDatabaseManager implements DatabaseManager {

	private Connection connection;
	private String DBNAME;
	
	public SQLiteDatabaseManager() throws CannotConnectException{
		connect("ishyou");
		try{
			Statement statement = null;
			statement = connection.createStatement();
			String sql = " CREATE TABLE IF NOT EXISTS users " +
	                   " (ID INTEGER PRIMARY KEY     AUTOINCREMENT," +
	                   " USERNAME           TEXT    NOT NULL, " + 
	                   " PASSWORD            TEXT     NOT NULL, " + 
	                   " TEAMID INT NOT NULL);" 
	                   +
	                   " INSERT INTO users (USERNAME, PASSWORD, TEAMID) " +
	                   " VALUES ('Admin', 'Password', 1);" 
	                   +
	                   " CREATE TABLE IF NOT EXISTS issues " +
	                   " (ID INTEGER PRIMARY KEY     AUTOINCREMENT," +
	                   " STATUS           TEXT    NOT NULL, " + 
	                   " TITLE            TEXT     NOT NULL, " + 
	                   " CONTENT TEXT NOT NULL);"
	                   +
	                   " CREATE TABLE IF NOT EXISTS issues_users " +
	                   " (USER_ID INTEGER NOT NULL," +
	                   " ISSUE_ID INTEGER NOT NULL);";
			statement.executeUpdate(sql);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean connect(String dataSourceName) throws CannotConnectException {
		DBNAME = dataSourceName;
		connection = null;
		File db = new File(dataSourceName + ".db");
		if (db.exists() && !db.isDirectory()){
			try{
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + dataSourceName + ".db");
				return true;
			} catch (Exception e){
				System.err.println( e.getClass().getName() + ": " + e.getMessage());
			}
		}
		
		throw new CannotConnectException();
	}
	

	@Override
	public ArrayList<Issue> getIssuesByUser(User user) {
		ArrayList<Issue> issues = new ArrayList<Issue>();
		try {
			connect(DBNAME);
			int userID = user.getUserID();
			Statement statement = connection.createStatement();
			ResultSet pivotTableResults = statement.executeQuery("SELECT ISSUE_ID FROM issues_users WHERE USER_ID = " + userID + ";");
			while (pivotTableResults.next()) {
				int issueID = pivotTableResults.getInt("ISSUE_ID");
				Statement statement2 = connection.createStatement();
				ResultSet issueTableResults = statement2.executeQuery("SELECT * FROM issues WHERE ID = " + issueID + ";");
				while (issueTableResults.next()){
					String statusString = issueTableResults.getString("STATUS");
					boolean complete = statusString.equals("COMPLETE");
					boolean pending = statusString.equals("PENDING");
					String title = issueTableResults.getString("TITLE");
					String content = issueTableResults.getString("CONTENT");
					issues.add(new Issue(issueID, complete, pending, title, content));
				}
			}
		} catch (CannotConnectException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return issues;
	}

	@Override
	public User authenticate(String username, String password)
			throws InvalidLoginDetailsException, CannotConnectException {
		
		if (isValidCredentials(username, password)){
			try {
				connect(DBNAME);
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE USERNAME = '" + username + "';");
				while( rs.next() ){
					int id = rs.getInt("ID");
					String correctPassword = rs.getString("PASSWORD");
					int teamID = rs.getInt("TEAMID");
					if (password.equals(correctPassword)){
						return new User(username, password, id, teamID);
					}
					rs.close();
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally{
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		throw new InvalidLoginDetailsException();
		
	}

	public boolean isValidCredentials(String username, String password) {
		return username.matches("[a-zA-Z]\\w+{3,}") && password.matches("\\w+");
	}

	@Override
	public boolean createUser(String username, String password, int teamID)
			throws CannotConnectException {
		if (isValidCredentials(username, password)){
			connect(DBNAME);
			try {
				Statement statement = connection.createStatement();
				String sql = "INSERT INTO users (USERNAME, PASSWORD, TEAMID) " +
							" VALUES ('" + username + "', '" + password + "', " + teamID + ");";
				statement.executeUpdate(sql);
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			} finally{
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return false;
	}
@Override
	public ArrayList<Issue> getAllIssues() throws CannotConnectException {
		connect(DBNAME);
		ArrayList<Issue> issues = new ArrayList<Issue>();
		try {
			Statement statement;
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM issues;");
			while( rs.next() ){
				int id = rs.getInt("ID");
				String statusString = rs.getString("STATUS");
				boolean complete = statusString.equals("COMPLETE");
				boolean pending = statusString.equals("PENDING");
				String title = rs.getString("TITLE");
				String content = rs.getString("CONTENT");
				issues.add(new Issue(id, complete, pending, title, content));
			}
			rs.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return issues;
	}

	@Override
	public boolean createIssue(String title, String content) throws CannotConnectException {
		connect(DBNAME);
		
		try {
			Statement statement = connection.createStatement();
			String sql = "INSERT INTO issues (STATUS, TITLE, CONTENT) " +
						" VALUES ('INCOMPLETE', '" + title + "', '" + content + "');";
			statement.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;		
	}

	@Override
	public boolean associateIssueWithUser(Issue issue, User user) throws CannotConnectException {
		connect(DBNAME);
		
		try {
			Statement statement = connection.createStatement();
			String sql = " INSERT INTO issues_users (USER_ID, ISSUE_ID) " +
						 " VALUES (" + user.getUserID() + "," + issue.getID() + ");";
			statement.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;		
	}

}

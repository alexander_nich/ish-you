package utils;

import domain.User;

public class Session {
	private User user;
	private boolean exists;
	
	public Session(User user) {
		this.user = user;
		this.exists = true;
	}

	public User getUser() {
		return this.user;
	}

	public boolean isValidSession() {
		return this.exists && this.hasUser();
	}

	private boolean hasUser() {
		if (this.user != null){
			return true;
		}
		return false;
	}

	public void delete() {
		this.exists = false;
		this.user = null;
	}

}

package utils;

import java.util.ArrayList;

import domain.Issue;
import domain.User;
import exceptions.CannotConnectException;
import exceptions.InvalidLoginDetailsException;

public interface DatabaseManager {
	
	public boolean connect(String dataSourceName) throws CannotConnectException;
	public ArrayList<Issue> getIssuesByUser(User user);
	public User authenticate(String username, String password) throws InvalidLoginDetailsException, CannotConnectException;
	public boolean createUser(String username, String password, int teamID) throws CannotConnectException;
	public boolean associateIssueWithUser(Issue issue, User user) throws CannotConnectException;
	public ArrayList<Issue> getAllIssues() throws CannotConnectException;
	public boolean createIssue(String title, String content) throws CannotConnectException;
	
}
